import { fileURLToPath, URL } from "node:url";

import { resolve } from "path";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import cssInjectedByJsPlugin from "vite-plugin-css-injected-by-js";

export default defineConfig({
  build: {
    rollupOptions: {
      output: {
        manualChunks: undefined,
      },
    },
    lib: {
      entry: resolve(__dirname, "src/main.ts"),
      name: "MyLib",
      formats: ["es"],
      fileName() {
        return "dsp-gui.js";
      },
    },
    minify: false,
  },
  plugins: [
    vue(),
    cssInjectedByJsPlugin({
      injectCode: (cssCode) => {
        return `window.getGlobalCss = () => ${cssCode};`;
      },
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  define: {
    "process.env": {},
  },
});
