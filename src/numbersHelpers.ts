export function numberToHumanReadableText(n: number, digits?: number): string {
  if (n < 10) {
    return n.toFixed(digits ?? 2);
  }

  if (n < 100) {
    return n.toFixed(digits ?? 1);
  }

  if (n < 1000) {
    return n.toFixed(digits ?? 0);
  }

  if (n < 10000) {
    return `${(n / 1000).toFixed(digits ?? 2)}k`;
  }

  if (n < 100000) {
    return `${(n / 1000).toFixed(digits ?? 1)}k`;
  }

  return `${(n / 1000).toFixed(digits ?? 0)}k`;
}
