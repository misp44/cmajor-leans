import { reactive, ref } from "vue";
import { cmajorAPI } from "./cmajor";

export interface IInputState {
  endpointID: string;
  type: string;
  value: unknown;
  label: string;
  unit?: string;
  min?: number;
  max?: number;
}

export interface IOutputState {
  endpointID: string;
  type: string;
  value: unknown;
  label: string;
  unit?: string;
  min?: number;
  max?: number;
}

export const store = reactive({
  name: "",
  primaryColor: "#333",
  secondaryColor: "#bbb",
  inputs: {} as Record<string, IInputState>,
  outputs: {} as Record<string, IOutputState>,
  error: undefined as string | undefined,
  setInput(id: string, value: unknown) {
    const input = this.inputs[id];

    if (!input) {
      return;
    }

    input.value = value;
    cmajorAPI.getConnection().sendEventOrValue(id, value);
  },
});

cmajorAPI.onConnection((connection) => {
  const valueChanged = (
    handler: string,
    endpointId: string,
    value: unknown
  ) => {
    // .setInput() cannot be used as it not only changes a state, but also send event to the patch
    const input = store.inputs[endpointId];

    if (!input) {
      return;
    }

    input.value = value;
  };

  const outputValueChanged = (
    handler: string,
    endpointId: string,
    value: unknown
  ) => {
    const output = store.outputs[endpointId];

    if (!output) {
      return;
    }

    output.value = value;
  };

  connection.addStatusListener((status) => {
    const { manifest, error, details } = status;
    const { inputs, outputs } = details;

    store.name = manifest.name;
    store.error = error;
    store.inputs = {};

    for (const input of inputs) {
      if (!input.annotation) {
        continue;
      }

      store.inputs[input.endpointID] = {
        endpointID: input.endpointID,
        type: input.annotation.type as string,
        label: input.annotation.name as string,
        unit: input.annotation.unit as string,
        value: input.annotation.init ?? 0,
        min: input.annotation.min as number,
        max: input.annotation.max as number,
      };

      connection.addParameterListener(input.endpointID, (value: unknown) =>
        valueChanged("onParameterEndpointChanged", input.endpointID, value)
      );

      connection.requestParameterValue(input.endpointID);
    }

    for (const output of outputs) {
      if (!output.annotation) {
        continue;
      }

      store.outputs[output.endpointID] = reactive({
        endpointID: output.endpointID,
        type: output.annotation.type as string,
        label: output.annotation.name as string,
        unit: output.annotation.unit as string,
        value: ref(output.annotation.init ?? 0),
        min: output.annotation.min as number,
        max: output.annotation.max as number,
      });

      connection.addEndpointListener(output.endpointID, (value: unknown) =>
        outputValueChanged("onEndpointEvent", output.endpointID, value)
      );

      connection.requestParameterValue(output.endpointID);
    }
  });

  connection.requestStatusUpdate();
});
