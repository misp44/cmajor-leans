import { defineCustomElement } from "vue";
import App from "./App.vue";

import "./assets/main.css";
import { cmajorAPI, type IPatchConnection } from "./cmajor";

declare function getGlobalCss(): string;

// createApp(App).mount('#app')

const AppElement = defineCustomElement(App);

export default function createCustomPatchView(
  patchConnection: IPatchConnection
) {
  const element = new AppElement();

  const elementStyle = document.createElement("style");
  elementStyle.appendChild(document.createTextNode(getGlobalCss()));
  element.shadowRoot!.appendChild(elementStyle);

  cmajorAPI.setConnection(patchConnection);

  return element;
}

if (!window.customElements.get("custom-patch-view")) {
  window.customElements.define("custom-patch-view", AppElement);
}
