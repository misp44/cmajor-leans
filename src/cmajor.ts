export interface IPatchManifest {
  ID: string;
  verstion: string;
  name: string;
  description: string;
  category: string;
  manufacturer: string;
}

export interface IConnection {
  purpose: string;
  endpointID: string;
  annotation: Record<string, unknown>;
}

export interface IInput extends IConnection {}
export interface IOutput extends IConnection {}

export interface IPatchDetails {
  inputs: IInput[];
  outputs: IOutput[];
}

export interface IPatchStatus {
  details: IPatchDetails;
  error: string;
  manifest: IPatchManifest;
  sampleRate: number;
}

export type StatusListener = (status: IPatchStatus) => void;
export type EndpointEventListener = (event: unknown) => void;
export type ParameterListener = (value: unknown) => void;

export interface IPatchConnection {
  sendEventOrValue(
    endpointID: string,
    value: unknown,
    numFrames?: number
  ): void;
  sendParameterGestureStart(endpointID: string): void;
  sendParameterGestureEnd(endpointID: string): void;

  requestStatusUpdate(): void;
  requestParameterValue(endpointID: string): void;

  addStatusListener(listener: StatusListener): void;
  addEndpointListener(
    endpointID: string,
    listener: EndpointEventListener,
    granularity?: number
  ): void;
  addParameterListener(endpointID: string, listener: ParameterListener): void;

  // onPatchStatusChanged?: (
  //   errorMessage: string | undefined,
  //   patchManifest: IPatchManifest,
  //   inputsList: IInput[],
  //   outputsList: IOutput[]
  // ) => void;
  // onSampleRateChanged?: (newSampleRate: number) => void;
  // onParameterEndpointChanged?: (endpointID: string, newValue: unknown) => void;
  // onEndpointEvent?: (endpointID: string, newValue: unknown) => void;
}

export class CMajorAPI {
  private _pathConnection: IPatchConnection | undefined;
  private _callbacks: ((connection: IPatchConnection) => void)[] = [];

  public setConnection(pathConnection: IPatchConnection) {
    this._pathConnection = pathConnection;
    this._callbacks.forEach((cb) => cb(pathConnection));
  }

  public onConnection(cb: (connection: IPatchConnection) => void): void {
    this._callbacks.push(cb);
  }

  public getConnection(): IPatchConnection {
    if (!this._pathConnection) {
      throw new Error("No patch connection");
    }

    return this._pathConnection;
  }
}

export const cmajorAPI = new CMajorAPI();

// @ts-ignore
window.cmajorAPI = cmajorAPI;
