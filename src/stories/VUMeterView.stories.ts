import type { Meta, StoryObj } from "@storybook/vue3";

import VUMeter from "../components/VUMeterView.vue";
import { store } from '../store';

store.outputs['id-1'] = {
  endpointID: 'id-1',
  type: 'vu',
  label: 'Example meter',
  unit: 'dB',
  value: 0,
  min: -40,
  max: 20,
};

const meta: Meta<typeof VUMeter> = {
  title: "CMajor Lens/VU Meter",
  component: VUMeter,
  render: (args: any) => ({
    components: { VUMeter },
    setup() {
      return { args };
    },
    template: `<VUMeter
			:endpointID="id-1"
			:units="dB"
		/>`,
  }),
  parameters: {},
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof VUMeter>;

export const VU: Story = {
  args: {
    units: 'dB',
    endpointID: 'id-1',
  },
};
