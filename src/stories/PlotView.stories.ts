import type { Meta, StoryObj } from "@storybook/vue3";

import PlotView from "@/components/PlotView.vue";
import { store } from '../store';

const POINTS = 50;

store.outputs['oscilloscope-endpoint-id'] = {
  endpointID: 'oscilloscope-endpoint-id',
  type: 'oscilloscope',
  label: 'Example meter',
  value: new Array(POINTS)
    .fill(undefined)
    .map((_, i) => Math.sin((i / (POINTS - 1)) * 2 * Math.PI)),
};

store.outputs['xy-endpoint-id'] = {
  endpointID: 'xy-endpoint-id',
  type: 'xy-oscilloscope',
  label: 'Example meter',
  value: new Array(POINTS)
    .fill(undefined)
    .flatMap((_, i) => [
      (i / (POINTS - 1)) * Math.sin((i / (POINTS - 1)) * 2 * Math.PI),
      (i / (POINTS - 1)) * Math.cos((i / (POINTS - 1)) * 2 * Math.PI),
    ]),
};

store.outputs['lissajous-endpoint-id'] = {
  endpointID: 'lissajous-endpoint-id',
  type: 'lissajous',
  label: 'Example meter',
  value: new Array(POINTS)
    .fill(undefined)
    .flatMap((_, i) => [
      (i / (POINTS - 1)) * Math.sin((i / (POINTS - 1)) * 2 * Math.PI),
      (i / (POINTS - 1)) * Math.cos((i / (POINTS - 1)) * 2 * Math.PI),
    ]),
};


const meta: Meta<typeof PlotView> = {
  title: "CMajor Lens/Plot",
  component: PlotView,
  render: (args: any) => ({
    components: { PlotView },
    setup() {
      return { args };
    },
    template: `<PlotView />`,
  }),
  parameters: {},
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof PlotView>;

export const Oscilloscope: Story = {
  args: {
    endpointID: 'oscilloscope-endpoint-id',
  },
};

export const XY: Story = {
  args: {
    endpointID: 'xy-endpoint-id',
  },
};

export const Lissajous: Story = {
  args: {
    endpointID: 'lissajous-endpoint-id',
  },
};
