import type { Meta, StoryObj } from "@storybook/vue3";

import BodePlotView from "@/components/BodePlotView.vue";
import { store } from '../store';

const POINTS = 500;

store.outputs['frequency-response-endpoint-id'] = {
  endpointID: 'frequency-response-endpoint-id',
  type: 'bode-plot',
  label: 'Freq. response',
  value: new Array(POINTS)
    .fill(undefined)
    .map((_, i) => Math.sin((i / (POINTS - 1)) * 2 * Math.PI)),
};

const meta: Meta<typeof BodePlotView> = {
  title: "CMajor Lens/Bode Plot",
  component: BodePlotView,
  render: (args: any) => ({
    components: { BodePlotView },
    setup() {
      return { args };
    },
    template: `<BodePlotView />`,
  }),
  parameters: {},
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof BodePlotView>;

export const FrequencyResponse: Story = {
  args: {
    endpointID: 'frequency-response-endpoint-id',
  },
};
