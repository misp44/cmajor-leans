import type { Meta, StoryObj } from "@storybook/vue3";

import KnobControl from "./../components/KnobControl.vue";
import { store } from '../store';

store.inputs['id-1'] = {
  endpointID: 'id-1',
  type: 'knob',
  label: 'Example knob',
  unit: 'dB',
  value: 0,
  min: -100,
  max: 100,
};

const meta: Meta<typeof KnobControl> = {
  title: "CMajor Lens/Knob",
  component: KnobControl,
  render: (args: any) => ({
    components: { KnobControl },
    setup() {
      return { args };
    },
    template: `<KnobControl
			:endpointID="id-1"
			:units="dB"
		/>`,
  }),
  parameters: {},
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof KnobControl>;

export const Knob: Story = {
  args: {
    units: 'dB',
    endpointID: 'id-1',
  },
};
