import type { Meta, StoryObj } from "@storybook/vue3";

import Meter from "../components/MeterView.vue";
import { store } from '../store';

store.outputs['id-1'] = {
  endpointID: 'id-1',
  type: 'vu',
  label: 'Example meter',
  unit: 'dB',
  value: 0,
  min: -40,
  max: 20,
};

const meta: Meta<typeof Meter> = {
  title: "CMajor Lens/Meter",
  component: Meter,
  render: (args: any) => ({
    components: { Meter },
    setup() {
      return { args };
    },
    template: `<Meter/>`,
  }),
  parameters: {},
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof Meter>;

export const VU: Story = {
  args: {
    units: 'dB',
    endpointID: 'id-1',
  },
};
