# CMajor Lens

<img src="assets/example_2_scr.png" width=400 />

Custom CMajor GUI for quick debugging and experimenting with your [CMajor](https://cmajor.dev/) patches.
CMajor lens receives output from your `[[main]]` processor and automatically generates a GUI based on your annotations.
You can use CMajor lens to display:
* Oscilloscope
* RMS and peak levels (digital and analog-style VU meters)
* FFT
* Frequency response
* Lissajous graph

## Adding GUI to your patch

First, download the latest `dsp-gui.js` from https://gitlab.com/misp44/cmajor-lens/-/packages/, or build from the repository with commands:

```sh
npm install

npm run build

# now you can find `dsp-gui.js` in `dist` directory
```

To use the GUI in your patch, copy the `dsp-gui.js` file to the directory with your patch and add the `view` definition to your `.cmajorpatch` file.

```json
    "view": {
        "src": "dsp-gui.js",
        "width": 500,
        "height": 600,
        "resizable": true
    }
```

## Instrumentation helpers

Instead of writing your own meters, you can use the ones already implemented in `Instrumentation.cmajor`.
```json
    "source": ["Example.cmajor", "Instrumentation.cmajor"]
```

## Usage

You can review how to use the CMajor lens with examples in `Example.cmajor` and `Filter.cmajor`.

<img src="assets/example_scr.png" width=400 />
<img src="assets/filter_scr.png" width=400 />
