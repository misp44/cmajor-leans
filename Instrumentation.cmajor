processor ValueEcho
{
	input stream float inValue [[ name: "Value In",  min: 0, max: 1.0, init:  0.0 ]];
	output event float outEvent;

	void main()
	{
		int count = 0;

		loop
		{
			if (++count >= processor.frequency / 25)
			{
				outEvent <- inValue;
				count = 0;
			}

			advance();
		}
	}
}

processor LevelMeter {
	input stream float in;
	output event float outEvent;

	void main()
	{
		int count = 0;

		float sum = 0;

		loop
		{
			sum += in * in;

			if (++count >= processor.frequency / 10)
			{
				float volume = 20 * log10(sqrt(sum / count));

				outEvent <- volume;

				count = 0;
				sum = 0;
			}

			advance();
		}
	}
}

processor PeakMeter {
	input stream float in;
	output event float outEvent;

	void main()
	{
		int eventsTimer = 0;
		int resetTimer = 0;

		float peak = 0.0f;
		bool dirty = false;

		loop
		{
			if (in > peak) {
				peak = in;

				resetTimer = 0;

				dirty = true;
			}

			if (++eventsTimer >= processor.frequency / 25)
			{
				float volume = 20 * log10(peak);

				if (dirty) {
					outEvent <- volume;
				}

				eventsTimer = 0;
				dirty = false;
			}

			if (++resetTimer >= processor.frequency)
			{
				peak = in;
				eventsTimer = 0;
				resetTimer = 0;
			}

			advance();
		}
	}
}

processor SnapshotGenerator
{
	input stream float in;
	output event float[snapshotLength] snapshot;

	let snapshotLength = 1024;
	let snapshotRate = 30.0f;

	void main()
	{
		var framesPerSnapshot = int (processor.frequency / snapshotRate);

		loop
		{
			float[snapshotLength] buffer;

			wrap<snapshotLength> i;
			loop (snapshotLength)
			{
				buffer[i++] = in;
				advance();
			}

			snapshot <- buffer;

			loop (framesPerSnapshot - snapshotLength)
				advance();
		}
	}
}

processor FFT
{
	input event float[snapshotLength] snapshot;
	output event float[snapshotLength/2] dftReal;
	output event float[snapshotLength/2] dftImaginary;
	output event float[snapshotLength/2] magnitude;

	let snapshotLength = 1024;

	// The output array receives the real and imaginary components
	float[snapshotLength] out;

	event snapshot(float[snapshotLength] e) {
		std::frequency::realOnlyForwardFFT(e, out);

		float[512] real = out[0:512];
		float[512] imaginary = out[512:1024];

		dftReal <- out[0:512];
		dftImaginary <- out[512:1024];

		float[512] sum;
		wrap<512> i = 0;
		loop (512) {
			// "/ 2 - 0.5f" is added to skale the FFT better in X/Y graph
			sum[i] = sqrt(real[i] ** 2 + imaginary[i] ** 2) / 2 - 0.5f;
			i++;
		}

		magnitude <-sum;
	}
}

processor SnapshotXYGenerator
{
	input stream float x;
	input stream float y;
	output event float[snapshotLength] snapshot;

	let snapshotLength = 1024;
	let snapshotRate = 30.0f;

	void main()
	{
		var framesPerSnapshot = int (processor.frequency / snapshotRate);

		loop
		{
			float[snapshotLength] buffer;

			wrap<snapshotLength> xi;
			wrap<snapshotLength> yi = 1;

			loop (snapshotLength / 2)
			{
				buffer[xi] = x;
				buffer[yi] = y;

				xi+= 2;
				yi+= 2;

				advance();
			}

			snapshot <- buffer;

			loop (framesPerSnapshot - snapshotLength)
				advance();
		}
	}
}

processor ImpulseTester {
	input stream float in;
	output stream float out;

	output event float[snapshotLength] snapshot;

	let snapshotLength = 1024;
	let snapshotRate = 30.0f;

	void main()
	{
		float sum = 0;
		int count = 0;

		wrap<32> snapshotIndex = 0;

		loop
		{
			var framesPerSnapshot = int (processor.frequency / snapshotRate);

			loop
			{
				float[snapshotLength] buffer;

				wrap<snapshotLength> i = 0;

				out <- 1;
				advance();

				loop (snapshotLength)
				{
					out <- 0;
					buffer[i] = in;
					i++;
					advance();
				}

				snapshot <- buffer;

				loop (framesPerSnapshot - snapshotLength)
					advance();
			}
		}
	}
}
